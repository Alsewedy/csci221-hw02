#include "encrypt.h"
#include <cstring>

/**
 * Constructor
 */
Encrypter::Encrypter() {

}

/**
 * Destructor.
 */
Encrypter::~Encrypter() {

}

/**
 * Encrypts the given input file according to given block size and permutation.
 * Writes the result to given output file.
 * Returns output text.
 */
std::string Encrypter::EncryptFile(std::fstream &inputFile,
		std::fstream &outputFile, int *permutation, int blockSize) {
	// Reset variables.
	inputText = "";
	outputText = "";
	inputStringStream.str("");
	inputStringStream.clear();
	outputStringStream.str("");
	outputStringStream.clear();

	// Check parameters
	if (permutation == NULL || blockSize <= 0) {
		return "ERROR";
	}

	// Initialize input and output buffers.
	char inputBuffer[blockSize + 1] = { 0 };
	char outputBuffer[blockSize + 1] = { 0 };

	// Read the content of the input file into inputStringStream ad set inputText.
	inputStringStream << inputFile.rdbuf();
	inputText = inputStringStream.str();
	
	// Calculate how many blocks are in the input text.
	int blockCount = inputText.length() / blockSize;
	// Calculate the amount of the padding
	int paddingCount = (blockSize - (inputText.length() % blockSize))
			% blockSize;

	// Encrypt the given input text.
	for (int i = 0; i < blockCount; i++) {
		// Buffer the input text.
		memcpy(inputBuffer, &(inputText.c_str()[i * blockSize]), blockSize);
		for (int j = 1; j <= blockSize; j++) {
			// Write permutation the input buffer into output buffer.
			outputBuffer[j - 1] = inputBuffer[permutation[j] - 1];
		}

		// Append output buffer to the outputText
		outputText.append(outputBuffer);
	}

	// Check if padding is needed and handle padding if needed.
	if (paddingCount > 0) {
		memcpy(inputBuffer, &(inputText.c_str()[blockCount * blockSize]),
				blockSize - paddingCount);
		for (int i = 0; i < paddingCount; i++) {
			inputBuffer[blockSize - paddingCount + i] = 'x';
		}

		for (int j = 1; j <= blockSize; j++) {
			outputBuffer[j - 1] = inputBuffer[permutation[j] - 1];
		}

		outputText.append(outputBuffer);
	}

	// Write output text to output file.
	outputStringStream.str(outputText);
	outputFile << outputStringStream.rdbuf();

	return outputText;
}

