#include <iostream>
#include <sstream>
#include <fstream>
#include <cstring>
#include "encrypt.h"
#include "decrypt.h"

// Global constants
#define NUMBER_OF_ARGUMENTS 6
#define MAX_BLOCKSIZE 8
#define MIN_BLOCKSIZE 2

// Options
static const char *ModeEncryption = "-e";
static const char *ModeDecryption = "-d";
static const char *InputFileOption = "-i";
static const char *OutputFileOption = "-o";

// Enumeration of modes.
enum Modes {
	None = 0, Encription, Decryption
};

/**
 * Returns the corresponding mode of the given string.
 */
Modes GetMode(const char *modeArg) {
	if (strcmp(modeArg, ModeEncryption) == 0) {
		return Encription;
	} else if (strcmp(modeArg, ModeDecryption) == 0) {
		return Decryption;
	} else {
		return None;
	}
}

/**
 * Read a file name from command line arguments according to entered option.
 */
bool GetFileName(int argIndex, char *argv[], const char *&inputFileName,
		const char *&outputFileName) {
	if (strcmp(argv[argIndex], InputFileOption) == 0) {
		inputFileName = argv[argIndex + 1];
	} else if (strcmp(argv[argIndex], OutputFileOption) == 0) {
		outputFileName = argv[argIndex + 1];
	} else {
		return false;
	}

	return true;
}

/**
 * Reads user input from stdin and returns it as a string.
 */
void GetString(std::string &input) {
	std::cout.flush();

	while (true) {
		try {
			std::getline(std::cin, input);
			break;
		} catch (std::exception &e) {
			std::cout << "Invalid user input. Please retry: ";
		}
	}
}

/**
 * Breaks up the given permutation value into its digits and checks if it is valid.
 * Return the permutation as an array of integers if valid. Otherwise, returns NULL.
 */
int* ParsePermutationValue(int blockSize, int permutationValue) {
	int *permutation = new int[blockSize + 1];
	unsigned int digits[blockSize + 1][2];
	bool isValid = true;

	permutation[0] = 0;

	// Set all digits except zero to zero meaning absent in the permutation.
	for (int i = 1; i <= blockSize; i++) {
		digits[i][0] = i;
		digits[i][1] = 0;
	}

	// Read permutation value digit by digit.
	for (int i = blockSize; i > 0; i--) {
		permutation[i] = permutationValue % 10;
		permutationValue = permutationValue / 10;

		// Set the read digit to 1 meaning it exists in the permutation.
		digits[permutation[i]][1] = 1;
	}

	// Check if all numbers from 1 to blockSize exists in the permutation.
	for (int i = 1; i <= blockSize; i++) {
		if (!digits[i][1]) {
			isValid = false;
			break;
		}
	}

	// If the permutation value is not valid
	// free permutation array and return NULL.
	if (!isValid) {
		delete[] permutation;
		return NULL;
	}

	return permutation;
}

/**
 * Gets block size and permutation from the user via stdin.
 */
void GetBlockSizeAndPermutation(int &blockSize, int *&permutation) {
	std::string line;
	std::string temp;
	std::stringstream stringStream;
	int permutationValue = 0;

	// Loop until user provides valid values for both block size and permutation.
	while (true) {
		std::cout
				<< "Please enter the block size (2-8) and the permutation (e.g., 4 2413): ";
		std::cout.flush();
		std::cin >> blockSize >> permutationValue;
		if (blockSize < MIN_BLOCKSIZE || blockSize > MAX_BLOCKSIZE) {
			std::cout << "Block size (" << blockSize
					<< ") is outside of allowed limits.\n";
			continue;
		}

		permutation = ParsePermutationValue(blockSize, permutationValue);
		if (permutation == NULL) {
			std::cout << "Invalid permutation!\n";
			continue;
		}

		break;
	}
}

/**
 * Open the file in the given path in given mode.
 * Returns true if successful, false otherwise.
 */
bool OpenFile(std::fstream &file, const char *filePath,
		std::ios::openmode mode) {
	file.open(filePath, mode | std::ios::binary);

	if (!file.is_open()) {
		return false;
	}

	return true;
}

/**
 * Main function of the program.
 */
int main(int argc, char *argv[]) {
	Modes mode = None;
	int blockSize = 0;
	int *permutation = NULL;
	const char *inputFileName = NULL;
	const char *outputFileName = NULL;
	std::fstream inputFile;
	std::fstream outputFile;
	Encrypter encrypter;
	Decrypter decrypter;

	// Check number of arguments.
	if (argc != NUMBER_OF_ARGUMENTS) {
		std::cout << "Invalid number of arguments!\n"
				<< "Expected number of arguments is " << NUMBER_OF_ARGUMENTS
				<< " but were provided " << argc << " argument(s).\n";
		return 1;
	}

	// Greet the user.
	std::cout << "Welcome to the Permutation Cipher\n";

	// Read mode from command line arguments.
	mode = GetMode(argv[1]);
	if (mode == None) {
		std::cout
				<< "Invalid option! Please choose \'-e\' for encryption of \'-d\' for decryption.\n";
		return 1;
	}
	
	std::cout << "Selected Mode: " << (mode == Encription ? "Encrypt" : "Decrypt") << "\n";

	// Read a filename either for input or output according to the preceding option.
	if (!GetFileName(2, argv, inputFileName, outputFileName)) {
		std::cout
				<< "Invalid option! Please choose \'-i\' for input file of \'-o\' for output file.\n";
		return 1;
	}

	// Read the next filename either for input or output according to the preceding option.
	if (!GetFileName(4, argv, inputFileName, outputFileName)) {
		std::cout
				<< "Invalid option! Please choose \'-i\' for input file of \'-o\' for output file.\n";
		return 1;
	}

	// Open input file.
	if (!OpenFile(inputFile, inputFileName, std::ios::in)) {
		std::cout << "Failed to open the input file(" << inputFileName
				<< ").\n";
		return 1;
	}
	
	std::cout << "Input File: " << inputFileName << "\n";

	// Open output file.
	if (!OpenFile(outputFile, outputFileName, std::ios::out)) {
		std::cout << "Failed to open the output file(" << outputFileName
				<< ").\n";
		inputFile.close();
		return 1;
	}
	
	std::cout << "Output File: " << outputFileName << "\n";

	// Get block size and permutation values from the user.
	GetBlockSizeAndPermutation(blockSize, permutation);

	// Perform requested operation.
	if (mode == Encription) {
		std::cout << "Encrypted ciphertext file: "
				<< encrypter.EncryptFile(inputFile, outputFile, permutation,
						blockSize) << "\n";
	} else {
		std::cout << "Decrypted ciphertext file: "
				<< decrypter.DecryptFile(inputFile, outputFile, permutation,
						blockSize) << "\n";
	}

	// If any memory is allocated to permutation free it.
	if (permutation != NULL) {
		delete[] permutation;
	}

	return 0;
}

