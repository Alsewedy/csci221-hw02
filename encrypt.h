#ifndef ENCRYPT_H_
#define ENCRYPT_H_

#include <fstream>
#include <sstream>
#include <string>

class Encrypter {
public:
	Encrypter();
	~Encrypter();

	std::string EncryptFile(std::fstream &inputFile, std::fstream &outputFile, int *permutation, int blockSize);

private:
	std::stringstream inputStringStream;
	std::stringstream outputStringStream;
	std::string inputText;
	std::string outputText;
};

#endif // ENCRYPT_H_

