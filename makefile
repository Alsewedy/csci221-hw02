CXX=g++ -std=c++11
RM=rm -f
OBJS=main.o encrypt.o decrypt.o

all: PermCipher

PermCipher: $(OBJS)
	$(CXX) -o PermCipher $(OBJS)
	
main.o: main.cpp encrypt.h decrypt.h
	$(CXX) -c main.cpp -o $@
	
encrypt.o: encrypt.cpp
	$(CXX) -c encrypt.cpp -o $@
	
decrypt.o: decrypt.cpp
	$(CXX) -c decrypt.cpp -o $@
	
clean:
	$(RM) PermCipher $(OBJS)