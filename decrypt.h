#ifndef DECRYPT_H_
#define DECRYPT_H_

#include <fstream>
#include <sstream>
#include <string>

class Decrypter {
public:
	Decrypter();
	~Decrypter();

	std::string DecryptFile(std::fstream &inputFile, std::fstream &outputFile, int *permutation, int blockSize);

private:
	std::stringstream inputStringStream;
	std::stringstream outputStringStream;
	std::string inputText;
	std::string outputText;
};

#endif // DECRYPT_H_

