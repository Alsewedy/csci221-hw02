#include "decrypt.h"
#include <cstring>

/**
 * Constructor
 */
Decrypter::Decrypter() {

}

/**
 * Destructor.
 */
Decrypter::~Decrypter() {

}

/**
 * Decrypts the given input file according to given block size and permutation.
 * Writes the result to given output file.
 * Returns output text.
 */
std::string Decrypter::DecryptFile(std::fstream &inputFile,
		std::fstream &outputFile, int *permutation, int blockSize) {
	// Reset variables.
	inputText = "";
	outputText = "";
	inputStringStream.str("");
	inputStringStream.clear();
	outputStringStream.str("");
	outputStringStream.clear();

	// Check parameters
	if (permutation == NULL || blockSize <= 0) {
		return "ERROR";
	}

	// Initialize input and output buffers.
	char inputBuffer[blockSize + 1] = { 0 };
	char outputBuffer[blockSize + 1] = { 0 };

	// Read the content of the input file into inputStringStream ad set inputText.
	inputStringStream << inputFile.rdbuf();
	inputText = inputStringStream.str();

	// Calculate how many blocks are in the input text.
	int blockCount = inputText.length() / blockSize;

	// Decrypt the given input text.
	for (int i = 0; i < blockCount; i++) {
		// Buffer the input text.
		memcpy(inputBuffer, &(inputText.c_str()[i * blockSize]), blockSize);
		for (int j = 0; j < blockSize; j++) {
			// Write permutation the input buffer into output buffer.
			outputBuffer[permutation[j + 1] - 1] = inputBuffer[j];
		}

		// Append output buffer to the outputText
		outputText.append(outputBuffer);
	}

	// Remove padding if any.
	for (int i = 1; i < blockSize; i++) {
		if (outputText[blockSize * blockCount - i] != 'x') {
			break;
		}

		outputText.pop_back();
	}

	// Write output text to output file.
	outputStringStream.str(outputText);
	outputFile << outputStringStream.rdbuf();

	return outputText;
}

